#ifndef _CHESSBOARD_
#define _CHESSBOARD_

#include <optimizers/optimizer.h>

#include <stack>
#include <vector>
#include <string>
#include <memory>
#include <list>

namespace Chess
{
	// Status of a square - either the occupying player or empty
	enum Player
	{
		Empty,
		First,
		Second
	};

	// describes a play to be made
	struct Square
	{
		Player player{Player::Empty};
		int x{0}, y{0}, id{0}, diagA{0}, diagB{0};
		int xMask{0}, yMask{0}, diagAMask{0}, diagBMask{0};
		double d{0};
	};

	inline bool operator>(const Square& lhs, const Square& rhs)
	{
		return lhs.id > rhs.id;
	}

	inline bool operator<(const Square& lhs, const Square& rhs)
	{
		return lhs.id < rhs.id;
	}

	inline bool operator==(const Square& lhs, const Square& rhs)
	{
		return lhs.id == rhs.id;
	}

	class ChessBoard
	{
	public:
		// creates a new chess grid
		ChessBoard();
		ChessBoard(int xSize, int ySize, bool rotOpt, bool isoOpt);
		ChessBoard(const ChessBoard& copyFrom);

		// print out the board configuration
		std::string print();
		
		// applies play on the board
		void apply(Square toApply);
		
		// undoes the last play on the board
		void undo();

		// gets the possible moves for whomever's turn it is
		std::vector<Square> getPossiblePlays();
		
		// Current turn's player
		Player Turn{Player::First};

		// The number of plays on this board currently
		unsigned int playCount{0};

		// Adds an optimizer, not in use at the moment
		void add_optimizer(std::shared_ptr<Optimizer> opt) { _optimizers.push_back(opt); }
		
		// Returns a Chessboard with a play applied that's not been tried yet
		std::unique_ptr<ChessBoard> getDonation(int playCountToUndo);

		// running maximum play count
		unsigned int maxPlayCount;
	private:
		// Returns line of sight status from one square to another
		static bool LineOfSight(const Square& a, const Square& b);

		// board property
		int xSize, ySize, numSquares;
		
		// Optimization flags, not in use at the moment
		bool rotOpt, isoOpt;

		// a vector of the available optimizers, not in use at the moment
		std::vector<std::shared_ptr<Optimizer>> _optimizers;

		// Optimizes a playlist, getting rid of plays which aren't necessary to be explored
		void optimizer(std::vector<Square>& playList);
		
		// references for line of sight checking
		inline int& colRef(Player player, int spaceId)
		{
			return columns[(player == Player::Second ? xSize : 0) + boardSpace[spaceId].x];
		}
		inline int& rowRef(Player player, int spaceId)
		{
			return rows[(player == Player::Second ? ySize : 0) + boardSpace[spaceId].y];
		}
		inline int& diagARef(Player player, int spaceId)
		{
			return diagAs[(player == Player::Second ? xSize+ySize : 0) + boardSpace[spaceId].diagA];
		}
		inline int& diagBRef(Player player, int spaceId)
		{
			return diagBs[(player == Player::Second ? xSize+ySize : 0) + boardSpace[spaceId].diagB];
		}

		// List of changes applied to the board
		std::vector<int> plays;

		// List of squares on the board with their respective states
		std::vector<Square> boardSpace;

		// helper vectors
		std::vector<Square> temp2;
		std::vector<std::vector<float>> temp3;
		std::vector<float> temp4;

		std::vector<int> rows;
		std::vector<int> columns;
		std::vector<int> diagAs;
		std::vector<int> diagBs;
		
		const static Player nextPlayer[3];

		// board distances look up table
		std::vector<std::vector<float>> boardSpaceDistances;
	};

	// Computes if A is in Line of Sight of B
    inline bool ChessBoard::LineOfSight(const Square& a, const Square& b)
	{
		int dx = (b.x - a.x); // x differential
		int dy = (b.y - a.y); // y differential
		// const bool onDiagonal = (dx == dy) || (dx == -dy); // has |slope| 1
		// const bool OnVerticalOrHorizontal = (dx == 0) ^ (dy == 0); // is purely vertical or horizontal

		return ((dx == dy) | (dx == -dy)) | ((dx == 0) ^ (dy == 0)); // is in line of sight
    }
}
#endif
