#include "ChessBoard.h"

#include <optimizers/isometry.h>
#include <optimizers/permutation.h>

#include "QueensDFS.h"
#include "PlayManager.h"

#include <iostream>
#include <fstream>
#include <ctime>
#include <vector>
#include <memory>
#include <thread>
#include <chrono>
#include <algorithm>    // std::reverse

/* Theory of operation:
	From the perspective an AI system. This is a depth-first search.
	
	> Create a game board
	> Put all move possibilities into a stack on the plays stack
	> While there are plays, do the following:
		> remove top possibility of plays stack, apply it to the board
		> put all new possibilies into a stack on the plays stack
		> if possibilies is empty, undo a move from board and pop the empty possibilites stack
	> The longest game configuration is recorded/displayed and the count of pair-wise moves reported.
*/

struct Output
{
	std::uint64_t exploredCount;
	Chess::ChessBoard maxBoard;
};

void thread_func(const Chess::Config& config,
				 const std::vector<Chess::Square>& initialPlays,
				 Output& output);

int main(int argc, char** argv)
{
	using namespace Chess;
	using namespace std;

	int n{0};
	bool rotOpt{false}, isoOpt{false}, permOpt{false};

	// default to 1 thread
	unsigned int num_threads{1};

	int i{0}, skip{0};
	for (i = 1; i < argc; ++i) {
		std::string arg = argv[i];

		if (skip)
		{
			skip--;
			continue;
		}

		if (arg == "-j")
		{
			skip = 1;
			if (i+1 >= argc)
			{
				std::cerr << "Error: '-j' requires an integer argument to specify number of threads" << std::endl;
				std::exit(1);
			}
			num_threads = std::stoi(argv[i+1]);
		}
		else if (arg == "--perm")
		{
			// permutation optimizations enabled
			permOpt = true;
		}
		else if (arg == "--iso")
		{ /* isometry optimizations enabled*/ }
		else if (i == 1)
		{
			// this argument assumed to be board size
			n = std::stoi(arg);
		}
		else
		{
			std::cerr << "Error: unknown CLI argument: " << arg << std::endl;
			std::exit(1);
		}
	}

	if (n == 0)
	{
		std::cerr << "Error: need a board size" << std::endl;
		std::exit(1);
	}

	// Get the first set of plays and round-robin assign those to threads
	ChessBoard init(n, n, rotOpt, isoOpt);

	if (permOpt)
		init.add_optimizer(std::make_shared<PermutationOptimizer>());
	
	auto initPlays = init.getPossiblePlays();

	int thread{0};
	std::vector<std::vector<Square>> to_distribute(num_threads);

	// separate into a distribution list for threads
	while (initPlays.empty() == false)
	{
		to_distribute[thread].push_back(initPlays.back());
		initPlays.pop_back();
		thread = ++thread % num_threads;
	}
	
	// reverse all the plays across the threads to keep monotonically increasing id order
	for (thread = 0; thread < num_threads; thread++)
		std::reverse(to_distribute[thread].begin(),to_distribute[thread].end());


	// Generate a config to send to the threads
	Config config;
	config.boardSize = n;
	config.isometryOptimization = isoOpt;
	config.rotationOptimization = rotOpt;
	config.permutationOptimization = permOpt;
	PlayManager pm; // make the main thread the owner of this object...someone needs to
	config.PM = &pm;

	// Create space for each thread to put its max config
	std::vector<Output> outputs(num_threads);

	// start the timer
	auto start = std::chrono::high_resolution_clock::now();

	// start the threads with their respective set of plays
	std::vector<std::thread> threads;

	// kick off and store a bunch of new threads
	for (unsigned int i{0}; i < num_threads; ++i)
		threads.push_back(std::thread(thread_func, config, to_distribute[i], std::ref(outputs[i])));

	// Wait for all the threads to finish
	for (auto& thread : threads)
		thread.join();

	// Compute elapsed time
	auto stop = std::chrono::high_resolution_clock::now();
	auto elapsedDuration = std::chrono::duration<double>(stop - start);

	// Get the final output stats
	ChessBoard maxBoard;
	std::uint64_t exploredCount{0};
	for (auto &output : outputs)
	{
		exploredCount += output.exploredCount; // update the explored count
		maxBoard = (output.maxBoard.playCount >= maxBoard.playCount ? output.maxBoard : maxBoard); // update the max board if found a bigger playCount
	}

	string outputString;
	outputString = "\nBoard Size: " + to_string(config.boardSize) + "x" + to_string(config.boardSize) + '\n';
	outputString += "Configurations Explored: " + to_string(exploredCount) + '\n';
	outputString += "Time: " + std::to_string(elapsedDuration.count()) + " [s]\n";
	outputString += "Final Configurations\n";
	outputString +=  maxBoard.print();

	std::cout << outputString;
}

void thread_func(const Chess::Config& config,
				 const std::vector<Chess::Square>& initialPlays,
				 Output& output)
{
	using namespace Chess;

	QueensDFS game(config, initialPlays);

	// start the clock!
	auto start = std::chrono::high_resolution_clock::now();
	clock_t startTime = clock();
	
	while (game.Play(1000)); // Play the game in chunks of plays at a time...arbitrary size

	// stop the clock!
	auto stop = std::chrono::high_resolution_clock::now();
	auto elapsedDuration = std::chrono::duration<double>(stop - start);

	auto tid = std::this_thread::get_id();
	std::cout << "Time for thread " << tid << ": " << elapsedDuration.count() << " [s]" << std::endl;

	output.exploredCount = game.GetExploredCount();
	output.maxBoard = game.GetMaxBoard();

	return;
}
