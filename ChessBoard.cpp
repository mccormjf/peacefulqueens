#include "ChessBoard.h"
#include <optimizers/optimizer.h>
#include <optimizers/isometry.h>

#include <stack>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <limits>
#include <list>

namespace Chess
{
	const Player ChessBoard::nextPlayer[] = {Player::Empty, Player::Second, Player::First};// turn transition matrix, included empty to avoid math
	
	// placeholder constructor
	ChessBoard::ChessBoard() : xSize(0), ySize(0), rotOpt(false), isoOpt(false) {}

	// normal constructor
	ChessBoard::ChessBoard(int xSize, int ySize, bool rotOpt, bool isoOpt)
	{
		// dimension the board
		this->xSize = xSize;
		this->ySize = ySize;

		// optimization settings, generalized isometry optimization
		// can replace rot...if it works, it's still under testing
		this->rotOpt = rotOpt;
		this->isoOpt = isoOpt;

		this->maxPlayCount = 0;
		
		// spiral loop
		/**
		 * Why spiral loop? Starting in the middle of the board and moving
		 * outwards, combinatorial possibilities start small and increase with
		 * each ring of squares. By choosing to play the combinatorially sparse
		 * region sooner, we can disqualifiy larger branches of the search's
		 * play-tree at once.
		 */
		int n = xSize;
		numSquares = n*n;

		int id = numSquares - 1;

		int x = n - 1;
		int y = n - 1;

		// pre-allocate all the vectors
		boardSpace.resize(numSquares);
		boardSpaceDistances.resize(numSquares);
		temp2.resize(numSquares);
		temp3.resize(numSquares);
		temp4.resize(numSquares);

		rows.resize(xSize * 2);
		columns.resize(ySize * 2);
		diagAs.resize((xSize+ySize) * 2);
		diagBs.resize((xSize+ySize) * 2);

		// Helper lambda for generating the loop:
		/**
		 * dx and dy control the change of position on the board per step in the loop
		 * n controls how many steps are in the segment
		 * x and y are the starting x and y coordinates
		 */
		auto spiarlLoopSegment = [](int &x, int &y, int &id, int &n, std::vector<Square> &squares, size_t dx, size_t dy)
		{
			for (int i{0}; i < n-1; (++i, x += dx, y+= dy, id--))
			{
				squares[id].id = id;
				squares[id].x = x;
				squares[id].y = y;
			}
		};


		while(n > 0)
		{
			// going up
			spiarlLoopSegment(x, y, id, n, boardSpace, 0, -1);

			// going left
			spiarlLoopSegment(x, y, id, n, boardSpace, -1, 0);
			
			// going down
			spiarlLoopSegment(x, y, id, n, boardSpace, 0, 1);

			// going right
			spiarlLoopSegment(x, y, id, n, boardSpace, 1, 0);

			// decrement n counter, square is smaller
			n -= 2;

			// move position up and left by one block.
			y--;
			x--;
		}

		// if dim was odd, fill in the middle square's info
		if (n < 0)
		{
			auto& square = boardSpace[0];
			square.id = 0;
			square.x = (xSize-1)/2;
			square.y = (xSize-1)/2;
		}

		// loop through nullspace and fill in metadata
		const float cX = ((float)xSize - 1.) / 2.;
		const float cY = cX;
		for (auto& s : boardSpace)
		{

			// Compute diagnols
			s.diagA = s.x + s.y;
			s.diagB = s.x + ySize - s.y;
			s.xMask = 1 << s.y;
			s.yMask = 1 << s.x;
			s.diagAMask = 1 << s.diagB;
			s.diagBMask = 1 << s.diagA;

			const float deltaX = s.x - cX;
			const float deltaY = s.y - cY;
			s.d = deltaX*deltaX + deltaY*deltaY; // center of the board distance

			// compute distance from s to all spaces
			// int dim = s.id * numSquares;
			boardSpaceDistances[s.id] = std::vector<float>(numSquares);
			for (auto& s2 : boardSpace)
			{
				float dx = s.x - s2.x;
				float dy = s.y - s2.y;
				boardSpaceDistances[s.id][s2.id] = (dx*dx + dy*dy);
			}
		}
	}
	
	// copy constructor
	ChessBoard::ChessBoard(const ChessBoard& copyFrom)
	{
		xSize = copyFrom.xSize;
		ySize = copyFrom.ySize;
		Turn = copyFrom.Turn;

		plays = copyFrom.plays;
		boardSpace = copyFrom.boardSpace;
		maxPlayCount = copyFrom.maxPlayCount;
		playCount = copyFrom.playCount;

		temp2 = copyFrom.temp2;
		temp3 = copyFrom.temp3;
		temp4 = copyFrom.temp4;

		rows = copyFrom.rows;
		columns = copyFrom.columns;
		diagAs = copyFrom.diagAs;
		diagBs = copyFrom.diagBs;

		boardSpaceDistances = copyFrom.boardSpaceDistances;
		numSquares = copyFrom.numSquares;
	}

	// apply and undo for board places
	void ChessBoard::apply(Square toApply)
	{
		playCount++;
		maxPlayCount = std::max(playCount, maxPlayCount);

		// nullspace is ordered by id, just set it to be occupied now
		boardSpace[toApply.id].player = Turn;

		plays.push_back(toApply.id); // add change
		
		int spaceID = plays.back();
		Player p = Turn;

		colRef(p, spaceID) ^= boardSpace[spaceID].xMask;
		rowRef(p, spaceID) ^= boardSpace[spaceID].yMask;
		diagARef(p, spaceID) ^= boardSpace[spaceID].diagAMask;
		diagBRef(p, spaceID) ^= boardSpace[spaceID].diagBMask;

		Turn = nextPlayer[Turn]; // change turns
	}
	
	void ChessBoard::undo()
	{
		playCount--;
		
		int spaceID = plays.back();
		Player p = nextPlayer[Turn]; // Undo the previous player's move

		colRef(p, spaceID) ^= boardSpace[spaceID].xMask;
		rowRef(p, spaceID) ^= boardSpace[spaceID].yMask;
		diagARef(p, spaceID) ^= boardSpace[spaceID].diagAMask;
		diagBRef(p, spaceID) ^= boardSpace[spaceID].diagBMask;

		boardSpace[plays.back()].player = Player::Empty;
		plays.pop_back();

		Turn = nextPlayer[Turn]; // change turns
	}

	// gets the possible moves for the turn
	std::vector<Square> ChessBoard::getPossiblePlays()
	{
        std::vector<Square>& newPlayList = temp2; // "Stack" of changes to return
		newPlayList.clear(); // clear the changes

		auto it = boardSpace.begin();
		if (plays.size() > 1)
		{
			// find the first element in boardSpace that is greater than the last play of this player
			// this will inherently be the lower bound of what needs checked each turn
			std::advance(it, plays[plays.size()-2] + 1);
		}

		// iterators to cutdown on access
		const auto bse = boardSpace.end();

		// for each square
		for ( ; it != bse; ++it)
		{
			Square& possible = *it;

			// Non-empty squares are not available squares
			if (possible.player != Player::Empty) continue;

			// check for opposite player's plays
			// Check diaganols first since they have more squares in them (more places for enemy pieces to be)
			if (diagARef(nextPlayer[Turn], possible.id)) continue;
			if (diagBRef(nextPlayer[Turn], possible.id)) continue;
			if (rowRef(nextPlayer[Turn], possible.id)) continue;
			if (colRef(nextPlayer[Turn], possible.id)) continue;

			newPlayList.push_back(possible); // Add play to possible list of plays
		}

		optimizer(newPlayList);// reverses newPlayList ordering and removes elements

		return newPlayList;
	}
	
	// Reverses playList ordering and removes elements to reduce the search-space
	inline void ChessBoard::optimizer(std::vector<Square>& playList)
	{
		constexpr float tolerance {std::numeric_limits<float>::epsilon() * 100};

		// Note: This function expects playList to be Square.id ordered
		// with lower id at bottom
		
		// a vector of vectors:
		// Each potential play gets an internal vector to store
		// distances from each potential play to all other played plays.
		std::vector<std::vector<float>> &distances = temp3;
		temp3.clear();

		// short-circuit...if count of plays for a player + number of new 
		// play possibilities for each player is less than the maxPlayCount,
		// we can stop considering any gameconfigs arising from the current
		// play. This compares maximum play-through potential for whichever
		// player is in play to the maximum seen play-through.
		// if second player, there can't be two more plays of each side
		unsigned int maxPlaysLeft = 2*playList.size() - (Turn == Player::Second);

		if (maxPlaysLeft < maxPlayCount - plays.size())
		{
			// if decidely less than maxPlayCount seen so far, return with
			// an empty stack of play possibilities
			// If plays left are less than the number of plays which were left at this point in the current maximal game
			playList.clear();
			return;
		}

		// Index for which to insert surviving plays
		int insertionIndex{0};
		for (int inspectingIndex{0}; inspectingIndex < playList.size(); inspectingIndex++)
		{
			Square &toCheck = playList[inspectingIndex];
			std::vector<float> &newPlaysDistances = temp4;
			newPlaysDistances.clear();

			/* Add a distance to center of board to have a
			 * board centric refernce point, any valid symmetry
			 * will have an  axis which passes through there.*/
			newPlaysDistances.push_back(toCheck.d);

			for (const auto &c : plays)
				newPlaysDistances.push_back(boardSpaceDistances[toCheck.id][c]);
			
			/* Compute distance from play to other existing plays.
			 * An isometry is any new possible play with an equivelant in
			 * distances to the existing plays.
			 * For each of the distances from a new possible play to
			 * another piece, compare the current new possible play's
			 * distances to the other distances.
			 * Equal ordered sets represent isometries.*/
			bool isometrical{false}; // if no distances in the set, the new play cannot be an isometry
			for (const std::vector<float> &c : distances) // check for isometries
			{
				// the following loop tests if the play isometrical with respect to the examined set.
				// There can only be either an isometry or not an isometry(a distance in the set is not equal)
				isometrical = true;

				for (std::size_t i{0}; isometrical & (i < newPlaysDistances.size()); ++i)
					isometrical ^= (std::abs(newPlaysDistances[i] - c[i]) > tolerance); // isometrical xor distance difference

				if (isometrical) break; // if the play's an isometry another, don't include the play
			}

			if (isometrical) continue; // if it's an isometry don't include it

			// Since only non-isometric plays are to be considered, a play should only
			// be added to the set of isometry-distance checks if the play's isometric status is novel in the set. 
			distances.push_back(newPlaysDistances);

			playList[insertionIndex++] = playList[inspectingIndex];
		}

		playList.resize(insertionIndex); // insertion index will be the same as size because element n+1 (where n = last element) = size
		
		// NOTE: reverses order of play possibilities
		// put new plays into playlist, a "stack" which should
		// try smaller items first(at top of "stack", back of list)
		std::reverse(playList.begin(), playList.end());
	}

	std::unique_ptr<ChessBoard> ChessBoard::getDonation(int playCountToUndo)
	{
		std::unique_ptr<ChessBoard> donorBoard = std::make_unique<ChessBoard>(*this); // make a copy of this board

		// take this board and make a copy and then undo moves until plays is done
		for (int n{0}; n < playCountToUndo; n++)
			donorBoard->undo();
		
		return donorBoard;
	}

	// prints out a representation of the board
	std::string ChessBoard::print()
	{
		// Header
		std::string ret{
			"-------------------------\n"
			"Board, Queen Pairs: "
		};

		ret += std::to_string(playCount / 2); // concatenate the count
		
		static const char spaceSymbol[3] {' ', 'b', 'w'}; // color matrix that decodes the Player enum
		
		// get the location in the string to start printing the board, include implicit future newline
		int boardStartIdx = ret.length() + 1;
		
		// each piece is represented by a string of "[_]", add newlines
		ret.resize(boardStartIdx + 3*numSquares + ySize, '\n');

		for (Square &q : boardSpace)
		{
			// index is where board starts + 3 chars for each piece in a fairly standard indexing pattern + number of newlines
			int idx = boardStartIdx + 3*(q.y * xSize + q.x) + q.y;
			
			// construct piece string
			std::string piece{"[_]"};
			piece[1] = spaceSymbol[q.player];
			
			// place the piece string at desired location
			ret.replace(idx, 3, piece);
		}

		ret += "-------------------------\n";
		return ret;
	}
}
