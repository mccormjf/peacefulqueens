#ifndef _PLAYMANAGER_
#define _PLAYMANAGER_

#include "ChessBoard.h"

#include <vector>
#include <string>
#include <memory>
#include <mutex>

namespace Chess
{
    // Each thread's QueensDFS gets one of these to interact with.
    // it's an input/output buffer for configurations being processed by the thread
    struct ThreadPlayList
    {
        // The main board used by QueensDFS
        std::unique_ptr<ChessBoard> mainBoard;

        // a board to be donated, threads may manage their donations until the donations are extracted
        std::unique_ptr<ChessBoard> donationBoard;
        
        // mutex for controlling access to donationBoard
        std::mutex donationBoardLock;
    };

	class PlayManager
    {
	public:
		PlayManager();
        // Attempts to fill ThreadPlayList with new plays from a donor under the manager. Returns successfulness.
        bool RequestPlays(ThreadPlayList& playlist);

        // Adds a ThreadPlayList to the manager so donors may be shared
        void AddThreadPlayList(ThreadPlayList& newList);

        // Removes a ThreadPlayList from the manager so it doesn't try to request donations from it anymore
        void RemoveThreadPlayList(ThreadPlayList& oldList);

        // Attempts to update the maximum seen play count among the managed ThreadPlayLists. Returns the maximum seen so far.
        int UpdateMaxPlayCount(int newMax);
	private:
        // The maximum seen play count among the managed ThreadPlayLists
        int maxPlayCount = 0;

        // This is the list of play lists for threads
        std::vector<ThreadPlayList*> masterList;
        
        // mutex for locking the named member
        std::mutex maxPlayCountLock, masterListLock;
	};
}
#endif
