#ifndef _QUEENSDFS_
#define _QUEENSDFS_

#include <optimizers/optimizer.h>
#include "ChessBoard.h"
#include "PlayManager.h"

#include <vector>
#include <string>
#include <memory>

namespace Chess
{
	struct Config
	{
		// The board size n by n
		int boardSize;

		// optimizer flags, currently unused
		bool rotationOptimization, isometryOptimization, permutationOptimization;
		
		// Accessor to the manager of the game
		PlayManager* PM;
	};

	class QueensDFS
	{
	public:
		QueensDFS(Config config);
		QueensDFS(Config config, const std::vector<Chess::Square>& initialPlays);
		~QueensDFS();

		// Plays a game for MaxConfigCount, and returns whether the game is finished
		bool Play(int MaxConfigCount = -1);

		// Returns the maximal board seen
		ChessBoard GetMaxBoard();

		// Returns the explored configurations count
		unsigned long long GetExploredCount();
	private:
		// Makes a donor board available if needed and possible
		void KeepDonorAvailable();

		// Explores the next play
		void Explore();

		// Undoes the last play
		void BackTrack();

		// Updates the status of the exploration within the DFS
		void UpdateExploredStatus();

		// Initialize everything except the plays to be played
		void Initialize(Config config);

		// Stores layers of ordered plays
		std::vector<std::vector<Square>> plays;
		
		// Contains pointers and any locks for the game boards
		ThreadPlayList PlayManagerPlaylist;

		// Accessor for the manager of this game
		PlayManager *manager;

		// The number of explored configurations
		unsigned long long exploredCount = 0;
		
		// The time the game started
		std::chrono::steady_clock::time_point startTime;
		
		// The current maximal game configuration
		ChessBoard MaxConfig;
		// notes that a play was tried before backtracking, if last iteration was a backtracking step then
		// a config must not be at a be a maximal state
		
		// Flag for having backtracked the last game step
		bool PlayedBeforeBackTracking = false;		
	};
}
#endif
