# let's just do the entire Peacable Queens in python, shall we?

import sys
import time

class Square():
    _x = -1
    _y = -1

    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def __repr__(self):
        return "Square{" + str(self._x) + "," + str(self._y) + "}"


class Color():
    WHITE = 0
    BLACK = 1
Colors = [Color.WHITE, Color.BLACK]


class Play():
    _player = None
    _square = None

    def __init__(self, player, square):
        self._player = player
        self._square = square

    @property
    def player(self):
        return self._player

    @property
    def square(self):
        return self._square


    def __repr__(self):
        color = 'White'
        if self._player == Color.BLACK:
            color = 'Black'
        return 'Play{' + color + ',' + repr(self._square) + '}'


class Board():
    # size of the board
    _n = 0

    # all plays made
    _plays = None

    # current player
    _currentPlayer = None
    
    # free spaces for each color
    _availableSquares = None

    # all free spaces (i.e. an entire board that is free)
    _allFreeSquares = None

    # go from (x, y) back to each Square object
    _invertedSquareDict = None


    def __init__(self, n):
        self._n = n

        self._allFreeSquares = set([Square(x, y) for y in range(n) for x in range(n)])

        self._invertedSquareDict = {}
        for square in self._allFreeSquares:
            self._invertedSquareDict[(square.x, square.y)] = square

        self._availableSquares = {}
        for color in Colors:
            self._availableSquares[color] = self._allFreeSquares.copy()

        self._plays = []

        self._currentPlayer = Color.WHITE


    def __str__(self):
        # create dict with inverted index of square to occupancy
        squareToOwner = {}
        for play in self._plays:
            squareToOwner[play.square] = play.player

        boardStr = ''
        for y in range(self._n):
            lineStr = ''
            for x in range(self._n):
                lineStr += '['
                square = self._invertedSquareDict[(x, y)]
                if square in squareToOwner:
                    if squareToOwner[square] == Color.WHITE:
                        lineStr += 'w'
                    else:
                        lineStr += 'b'
                else:
                    lineStr += ' '
                lineStr += ']'
            boardStr += lineStr + '\n'
        return boardStr


    def __getitem__(self, x, y):
        return self._invertedSquareDict[(x, y)]


    def copy(self):
        newBoard = Board(self._n)
        newBoard._plays = self._plays.copy()
        newBoard._allFreeSquares = self._allFreeSquares.copy()
        newBoard._invertedSquareDict = self._invertedSquareDict.copy()
        return newBoard


    def numPlays(self):
        return len(self._plays)


    def getPossiblePlays(self):
        # add culling step here
        return self._availableSquares[self._currentPlayer]


    def makePlay(self, square):
        self._plays.append(Play(self._currentPlayer, square))
        self._makePlay(self._currentPlayer, square)
        self._currentPlayer = Colors[self._currentPlayer - 1]

    def _makePlay(self, player, square):
        # remove the spot from current player's available squares
        self._availableSquares[player] = self._availableSquares[player] - set([square])

        # now remove all new danger squares from opponent's available squares
        opponent = Colors[player - 1]
        dangerZone = set()

        x = square.x
        y = square.y
        b = y - x
        for x in range(self._n):
            dangerZone.add(self._invertedSquareDict[(x, y)])
            if 0 <= x + b < self._n:
                dangerZone.add(self._invertedSquareDict[(x, x + b)])
        
        x = square.x
        y = square.y
        b = y + x
        for y in range(self._n):
            dangerZone.add(self._invertedSquareDict[(x, y)])
            x2 = b - y
            if 0 <= x2 < self._n:
                dangerZone.add(self._invertedSquareDict[(x2, y)])

        # remove danger zone from opponent's available squares
        self._availableSquares[opponent] = self._availableSquares[opponent] - dangerZone


    def undoPlay(self):
        if len(self._plays) == 0:
            raise RuntimeError("No plays available to undo")

        # grab and remove the last play
        lastPlay = self._plays.pop()

        # add this play's square back to the player's available list
        self._availableSquares[lastPlay.player] =\
                self._availableSquares[lastPlay.player] | set([lastPlay.square])

        # redefine the opponent's available square set with the new set of this player's plays
        opponent = Colors[lastPlay.player - 1]
        self._availableSquares[opponent] = self._allFreeSquares.copy()

        for play in self._plays:
            if play.player == lastPlay.player:
                self._makePlay(play.player, play.square)
            else:
                self._availableSquares[opponent] = \
                        self._availableSquares[opponent] - set([play.square])

        self._currentPlayer = lastPlay.player


def main(args):
    if len(args) == 0:
        print('need one arg for board size')
        return

    size = int(args[0])
    board = Board(size)

    biggestValidBoard = None

    # stack of sets of moves to try
    possiblePlayStack = list()

    # start off the first iteration
    possiblePlayStack.append(list(board.getPossiblePlays()))

    # keep doing the thing while there are sets of moves on the stack
    while len(possiblePlayStack) > 0:
        possiblePlays = possiblePlayStack[-1]

        if len(possiblePlays) > 0:
            # try another one of these moves
            move = possiblePlays.pop()

            # make the move
            board.makePlay(move)

            # add new moves to the stack
            possiblePlayStack.append(list(board.getPossiblePlays()))

        else:
            numPlays = board.numPlays()
            if numPlays % 2 == 0:
                if biggestValidBoard == None or numPlays > biggestValidBoard.numPlays():
                    biggestValidBoard = board.copy()

            # undo the last change
            if numPlays > 0:
                board.undoPlay()

            # discard this set of plays from the stack
            possiblePlayStack.pop()

    print('solution: m =', int(biggestValidBoard.numPlays() / 2))
    print(biggestValidBoard)


if __name__ == '__main__':
    main(sys.argv[1:])
