#ifndef _OPTIMIMIZERS_PERMUTATION_H_
#define _OPTIMIMIZERS_PERMUTATION_H_

#include <ChessBoard.h>

#include <optimizers/optimizer.h>

#include <stack>

namespace Chess {

/**
 * This optimizer culls permutations plays by requiring a monotonically
 * increasing square ID in the list of plays. Said differently, the set of
 * returned plays must have higher square IDs than previous plays by this
 * color.
 */
class PermutationOptimizer : public Optimizer {
public:
    PermutationOptimizer() = default;

    virtual void optimize(const std::vector<Square>& existingPlays, std::stack<Square>& newPlayList) override {
        // only modify the list if we have a previous play of the same color
        if (existingPlays.size() > 1) {
            auto& lastSameColorPlay = *(++(existingPlays.rbegin()));
            while (!newPlayList.empty()) {
                // require higher ID than last play
                auto& newPlay = newPlayList.top();
                if (newPlay.id > lastSameColorPlay.id) {
                    _survivors.push_back(newPlay);
                }
                newPlayList.pop();
            }

            // push the survivors back in the new play list go backward so that
            // IDs are still with smaller IDs at bottom of the stack
            for (auto survivor = _survivors.rbegin(); survivor != _survivors.rend(); ++survivor) {
                newPlayList.push(*survivor);
            }

            // cleanup
            _survivors.clear();
        }
    }

private:
    std::vector<Square> _survivors; // re-use the space we allocate

};

} // Chess

#endif /* _OPTIMIMIZERS_PERMUTATION_H_ */

