#ifndef _OPTIMIZERS_OPTIMIZER_H_
#define _OPTIMIZERS_OPTIMIZER_H_

#include <stack>
#include <vector>

namespace Chess {

// Forward declaration
class Square;

/**
 * Pure virtual class for optimizers
 */
class Optimizer {
public:
    virtual void optimize(const std::vector<Square>& existingPlays, std::stack<Square>& newPlayList) = 0;

};

} // Chess

#endif /* _OPTIMIZERS_OPTIMIZER_H_ */

