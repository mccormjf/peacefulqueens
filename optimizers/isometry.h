#ifndef _OPTIMIMIZERS_ISOMETRY_H_
#define _OPTIMIMIZERS_ISOMETRY_H_

#include <ChessBoard.h>

#include <optimizers/optimizer.h>

#include <stack>

namespace Chess {

class IsometryOptimizer : public Optimizer {
public:
    IsometryOptimizer() = default;

    virtual void optimize(const std::vector<Square>& existingPlays, std::stack<Square>& newPlayList) override {}

};

} // Chess

#endif /* _OPTIMIMIZERS_ISOMETRY_H_ */

