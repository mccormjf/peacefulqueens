EXE=PeacefulQueens

# let's put emphasis on debugging for now
CXXFLAGS=-g -Og -Wall -O2


SRCS=main.cpp \
	 ChessBoard.cpp \
	 QueensDFS.cpp \
	 PlayManager.cpp \

INCLUDES=ChessBoard.h \
	 QueensDFS.h \
	 PlayManager.h \

OBJS=$(SRCS:.cpp=.o)

DEPS=$(SRCS) $(INCLUDES)

.PHONY: all
all: $(EXE)

# generate dependencies
depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CXXFLAGS) -MM $^ -MF ./.depend

include .depend

PeacefulQueens: $(OBJS)
	$(CXX) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	rm -rf $(EXE) *.o *~ core .depend
