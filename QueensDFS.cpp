#include "QueensDFS.h"
#include <iostream>
#include <optimizers/isometry.h>
#include <optimizers/permutation.h>
#include "PlayManager.h"

namespace Chess
{
	QueensDFS::QueensDFS(Config config)
	{
		Initialize(config); // setup everything except initial plays
		manager->AddThreadPlayList(PlayManagerPlaylist);
		plays.push_back(PlayManagerPlaylist.mainBoard->getPossiblePlays()); // push the initial plays
	}
	
	QueensDFS::QueensDFS(Config config, const std::vector<Chess::Square>& initialPlays)
	{
		Initialize(config); // setup everything except initial plays
		plays.push_back(initialPlays); // push the initial plays
	}
	
	QueensDFS::~QueensDFS()
	{
		manager->RemoveThreadPlayList(PlayManagerPlaylist);
	}

	void QueensDFS::Initialize(Config config)
	{
		// configure the connection to the manager
		manager = config.PM;
		manager->AddThreadPlayList(PlayManagerPlaylist);

		// setup the board
		PlayManagerPlaylist.mainBoard = std::make_unique<ChessBoard>(config.boardSize, config.boardSize,
					 config.rotationOptimization, config.isometryOptimization);
		
		ChessBoard& board = *PlayManagerPlaylist.mainBoard;

		// Add optimizers
		// if (config.isometryOptimization)
		// 	board.add_optimizer(std::make_shared<IsometryOptimizer>());
		// if (config.permutationOptimization)
		// 	board.add_optimizer(std::make_shared<PermutationOptimizer>());

		startTime = std::chrono::steady_clock::now(); // start the clock on processing configs
	}

	ChessBoard QueensDFS::GetMaxBoard()
	{
		return MaxConfig;
	}

	unsigned long long QueensDFS::GetExploredCount()
	{
		return exploredCount;
	}

	inline void QueensDFS::Explore()
	{
		std::vector<Square>& movesToTry = plays.back(); // refer to the top-stock as movesToTry

		Square toApply = movesToTry.back(); // get change to apply...
		movesToTry.pop_back();

		ChessBoard& board = *PlayManagerPlaylist.mainBoard;
		board.apply(toApply); // apply a change
		
		plays.push_back(board.getPossiblePlays()); // get new plays
	}

	inline void QueensDFS::BackTrack()
	{
		ChessBoard& board = *PlayManagerPlaylist.mainBoard;

		if (board.playCount > 0)
			board.undo(); // undo current play

		if (plays.empty() == false)
			plays.pop_back(); // discard level of plays
	}

	inline void QueensDFS::UpdateExploredStatus()
	{
		exploredCount++; // completed exploring a new game config

		ChessBoard& board = *PlayManagerPlaylist.mainBoard;
		board.maxPlayCount = manager->UpdateMaxPlayCount(board.maxPlayCount);

		if (board.playCount > MaxConfig.playCount)
		{
			MaxConfig = board; // best board this thread has seen, update it

			if (board.playCount >= board.maxPlayCount)
			{
				// thread's best board is on par with maximal
				auto now = std::chrono::steady_clock::now();
				auto elapsedDuration = std::chrono::duration<double>(now - startTime);

				std::cout << "Time: " + std::to_string(elapsedDuration.count()) + " [s]\n";
				std::cout << board.print() << '\n';
				fflush(stdout);
			}
		}
	}

	inline void QueensDFS::KeepDonorAvailable()
	{
		// try getting a lock, but if that fails, try again later
		std::unique_lock<std::mutex> lock(PlayManagerPlaylist.donationBoardLock, std::try_to_lock);

		if (lock.owns_lock() && PlayManagerPlaylist.donationBoard == nullptr) // our donor config is missing
		{
			if (plays.empty() == false) // if we have configs that could be donated
			{
				Square aPlay;
				
				for (int n{0}; n < plays.size(); n++) // check each step of the game
				{
					auto & a = plays[n]; // move through the different layer of the stack
					
					if (a.empty()) continue; // continue to the next layer if we don't have a play to donate here
					
					aPlay = a[0];
					a.erase(a.begin()); // erase the first element, the one we've copied
					
					PlayManagerPlaylist.donationBoard = move(PlayManagerPlaylist.mainBoard->getDonation(plays.size() - n - 1));
					PlayManagerPlaylist.donationBoard->apply(aPlay);
					break;
				}
			}
		}
	}

	bool QueensDFS::Play(int PlaysToExplore)
	{
		// loop over game-space
		while (plays.empty() == false && PlaysToExplore-- > 0)
		{

			std::vector<Square>& movesToTry = plays.back(); // refer to the top-stock as movesToTry

			if (movesToTry.empty() == false){
				Explore(); // if we have moves to try, explore them

				KeepDonorAvailable(); // handle donations

				PlayedBeforeBackTracking = true; // exploring new plays
			}
			else
			{
				// If we explored a new config, update status on the first-player's turn (when second player was previous move)
				if (PlayedBeforeBackTracking && PlayManagerPlaylist.mainBoard->Turn == Player::First)
					UpdateExploredStatus();
				
				BackTrack();
				
				if (PlayedBeforeBackTracking && PlayManagerPlaylist.mainBoard->Turn == Player::First)
					UpdateExploredStatus();

				PlayedBeforeBackTracking = false; // in process of backtracking
			}
		}

		// get work if needed
		if (plays.empty())
		{
			if (manager->RequestPlays(PlayManagerPlaylist) == false)
				return false; // no work to be had
				
			PlayedBeforeBackTracking = true; // play has been made by donor
			plays.push_back(PlayManagerPlaylist.mainBoard->getPossiblePlays()); // get new plays
		}

		return true; // still has work to be done
	}
}
