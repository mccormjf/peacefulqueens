#include "PlayManager.h"
#include <algorithm>    // std::find

namespace Chess{
	PlayManager::PlayManager()
    {
	    const std::lock_guard<std::mutex> lock(masterListLock);
        masterList.clear();
	}
    
    bool PlayManager::RequestPlays(ThreadPlayList& playlist)
    {
	    const std::lock_guard<std::mutex> lock(masterListLock);
    
        for (const auto donor : masterList) // loop over the available donors
        {
		    std::unique_lock<std::mutex> lock(donor->donationBoardLock, std::try_to_lock); // try to lock onto donor
            
            if (lock.owns_lock() && donor->donationBoard != nullptr) // found an available donor...
            {
                playlist.mainBoard = move(donor->donationBoard); // move the board between games
                return true;
            }
        }

        return false; // no donations found...thread will decide what to do next
    }
    
    int PlayManager::UpdateMaxPlayCount(int newMax)
    {
        if (newMax > maxPlayCount)
        {
		    const std::lock_guard<std::mutex> lock(maxPlayCountLock);
            maxPlayCount = newMax;
            return newMax;
        }

        return maxPlayCount;
    }

    void PlayManager::RemoveThreadPlayList(ThreadPlayList& oldList)
    {
        const std::lock_guard<std::mutex> lock(masterListLock);
        auto it = find(masterList.begin(), masterList.end(), &oldList);
        masterList.erase(it);
    }

    void PlayManager::AddThreadPlayList(ThreadPlayList& newList)
    {
	    const std::lock_guard<std::mutex> lock(masterListLock);
        masterList.push_back(&newList); // store the new list in the manager
    }
}
